<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\PublishersRepositoryContract;

// Models
use App\Models\Publishers;

class PublishersEloquentRepository extends BaseEloquentRepository implements PublishersRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Publishers::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Publishers::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Publishers::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Publishers::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $publisher = Publishers::findOrFail($id);

        $publisher->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ])->save();

        return $publisher;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        return Publishers::findOrFail($id)->delete();
    }
}
