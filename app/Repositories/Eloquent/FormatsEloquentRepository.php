<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\FormatsRepositoryContract;

// Models
use App\Models\Formats;

class FormatsEloquentRepository extends BaseEloquentRepository implements FormatsRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Formats::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Formats::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Formats::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Formats::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $format = Formats::findOrFail($id);

        $format->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ])->save();

        return $format;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        return Formats::findOrFail($id)->delete();
    }
}
