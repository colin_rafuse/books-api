<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\BooksRepositoryContract;

// Models
use App\Models\Books;

class BooksEloquentRepository extends BaseEloquentRepository implements BooksRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Books::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function search($query)
    {
        return Books::with($this->requestedRelationships())->where('title', 'like', '%' . $query . '%')->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Books::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Books::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $book = Books::create([
            'slug' => array_get($data, 'slug'),
            'title' => array_get($data, 'title'),
            'description' => array_get($data, 'description'),
            'publisher_id' => array_get($data, 'publisher_id'),
            'genre_id' => array_get($data, 'genre_id'),
            'type_id' => array_get($data, 'type_id'),
            'format_id' => array_get($data, 'format_id'),
            'language_id' => array_get($data, 'language_id'),
            'pages' => array_get($data, 'pages'),
            'published_at' => array_get($data, 'published_at'),
            'isbn_ten' => array_get($data, 'isbn_ten'),
            'isbn_thirteen' => array_get($data, 'isbn_thirteen'),
        ]);

        $book->authors()->attach(array_get($data, 'author_id'));

        return $book;
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $book = Books::findOrFail($id);

        $book->fill([
            'slug' => array_get($data, 'slug'),
            'title' => array_get($data, 'title'),
            'description' => array_get($data, 'description'),
            'publisher_id' => array_get($data, 'publisher_id'),
            'genre_id' => array_get($data, 'genre_id'),
            'type_id' => array_get($data, 'type_id'),
            'format_id' => array_get($data, 'format_id'),
            'language_id' => array_get($data, 'language_id'),
            'pages' => array_get($data, 'pages'),
            'published_at' => array_get($data, 'published_at'),
            'isbn_ten' => array_get($data, 'isbn_ten'),
            'isbn_thirteen' => array_get($data, 'isbn_thirteen'),
        ])->save();

        $book->authors()->sync(array_get($data, 'author_id'));

        return $book;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        $book = Books::findOrFail($id);

        // NOTE: Update and Soft Delete the Author Pivot Records - Laravel doesn't have a method for Soft Deleting a Pivot Record!
        foreach ($book->authors as $author) {
            $book->authors()->updateExistingPivot([$author->id], ['deleted_at' => now()]);
        }

        $book->delete();

        return $book;
    }
}
