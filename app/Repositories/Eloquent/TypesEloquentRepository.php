<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\TypesRepositoryContract;

// Models
use App\Models\Types;

class TypesEloquentRepository extends BaseEloquentRepository implements TypesRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Types::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Types::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Types::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Types::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $type = Types::findOrFail($id);

        $type->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ])->save();

        return $type;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        return Types::findOrFail($id)->delete();
    }
}
