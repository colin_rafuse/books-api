<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\LanguagesRepositoryContract;

// Models
use App\Models\Languages;

class LanguagesEloquentRepository extends BaseEloquentRepository implements LanguagesRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Languages::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Languages::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Languages::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Languages::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'abbreviation' => array_get($data, 'abbreviation'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $language = Languages::findOrFail($id);

        $language->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'abbreviation' => array_get($data, 'abbreviation'),
        ])->save();

        return $language;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        return Languages::findOrFail($id)->delete();
    }
}
