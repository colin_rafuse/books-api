<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\GenresRepositoryContract;

// Models
use App\Models\Genres;

class GenresEloquentRepository extends BaseEloquentRepository implements GenresRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Genres::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Genres::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Genres::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Genres::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $genre = Genres::findOrFail($id);

        $genre->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ])->save();

        return $genre;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        return Genres::findOrFail($id)->delete();
    }
}
