<?php

namespace App\Repositories\Eloquent;

use Illuminate\Support\Facades\Log;

abstract class BaseEloquentRepository
{
    /**
     * The Request class instance.
     *
     * @var
     */
    protected $request;

    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct()
    {
        //
    }

    /**
     * {@inheritdoc}
     */
    public function shouldLoadRelationships()
    {
        return $this->request->has('embed') ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function requestedRelationships()
    {
        return $this->shouldLoadRelationships() ? (explode(',', $this->request->input('embed'))) : [];
    }
}
