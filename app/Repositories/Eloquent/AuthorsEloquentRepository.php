<?php

namespace App\Repositories\Eloquent;

use Illuminate\Http\Request;

// Contracts
use App\Repositories\Contracts\AuthorsRepositoryContract;

// Models
use App\Models\Authors;

class AuthorsEloquentRepository extends BaseEloquentRepository implements AuthorsRepositoryContract
{
    /**
     * Construct the class instance
     *
     * @param  \Illuminate\Http\Request  $request
     * @return  void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Authors::with($this->requestedRelationships())->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function search($query)
    {
        return Authors::with($this->requestedRelationships())->where('name', 'like', '%' . $query . '%')->paginate(config('domain-logic.pagination.items_per_page'));
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        return Authors::with($this->requestedRelationships())->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return (Authors::find($id)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Authors::create([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $author = Authors::findOrFail($id);

        $author->fill([
            'slug' => array_get($data, 'slug'),
            'name' => array_get($data, 'name'),
            'description' => array_get($data, 'description'),
        ])->save();

        return $author;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        $author = Authors::findOrFail($id);

        // NOTE: Update and Soft Delete the Authors Books Pivot Records - Laravel doesn't have a method for Soft Deleting a Pivot Record!
        foreach ($author->books as $book) {
            $author->books()->updateExistingPivot([$book->id], ['deleted_at' => now()]);
        }

        $author->delete();

        return $author;
    }
}
