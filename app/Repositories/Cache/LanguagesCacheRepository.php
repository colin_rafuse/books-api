<?php

namespace App\Repositories\Cache;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Repository;

// Contracts
use App\Repositories\Contracts\LanguagesRepositoryContract;

class LanguagesCacheRepository extends BaseCacheRepository implements LanguagesRepositoryContract
{
    /**
     * Construct the Cache Repository
     *
     * @param Request $request
     * @param LanguagesRepositoryContract $repository
     * @param Repository $cache
     * @return  void
     */
    public function __construct(Request $request, LanguagesRepositoryContract $repository, Repository $cache)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->all();
        }

        // Pull data out of cache, if it exists...
        return $this->cache->tags(['languages:all'])->remember('languages:all:page_' . $this->request->input('page', 1), config('domain-logic.cache.key_tty'), function() {

            // If cache has expired, grab out of the database
            return $this->repository->all();

        });
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->get($id);
        }

        // Pull data out of cache, if it exists...
        return $this->cache->remember('languages:' . $id, config('domain-logic.cache.key_tty'), function() use($id) {

            // If cache has expired, grab out of the database
            return $this->repository->get($id);

        });
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return $this->repository->exists($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $this->cache->tags(['languages:all'])->flush();

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $this->cache->tags(['languages:all'])->flush();
        $this->cache->forget('languages:' . $id);

        return $this->repository->update($id, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        $this->cache->tags(['languages:all'])->flush();
        $this->cache->forget('languages:' . $id);

        return $this->repository->delete($id);
    }
}
