<?php

namespace App\Repositories\Cache;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Repository;

// Contracts
use App\Repositories\Contracts\TypesRepositoryContract;

class TypesCacheRepository extends BaseCacheRepository implements TypesRepositoryContract
{
    /**
     * Construct the Cache Repository
     *
     * @param Request $request
     * @param TypesRepositoryContract $repository
     * @param Repository $cache
     * @return  void
     */
    public function __construct(Request $request, TypesRepositoryContract $repository, Repository $cache)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->all();
        }

        // Pull data out of cache, if it exists...
        return $this->cache->tags(['types:all'])->remember('types:all:page_' . $this->request->input('page', 1), config('domain-logic.cache.key_tty'), function() {

            // If cache has expired, grab out of the database
            return $this->repository->all();

        });
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->get($id);
        }

        // Pull data out of cache, if it exists...
        return $this->cache->remember('types:' . $id, config('domain-logic.cache.key_tty'), function() use($id) {

            // If cache has expired, grab out of the database
            return $this->repository->get($id);

        });
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return $this->repository->exists($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $this->cache->tags(['types:all'])->flush();

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $this->cache->tags(['types:all'])->flush();
        $this->cache->forget('types:' . $id);

        return $this->repository->update($id, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        $this->cache->tags(['types:all'])->flush();
        $this->cache->forget('types:' . $id);

        return $this->repository->delete($id);
    }
}
