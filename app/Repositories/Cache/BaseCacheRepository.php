<?php

namespace App\Repositories\Cache;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Repository;

abstract class BaseCacheRepository
{
    /**
     * The Request class instance.
     *
     * @var
     */
    protected $request;

    /**
     * The Repository class instance.
     *
     * @var
     */
    protected $repository;

    /**
     * The Cache class instance.
     *
     * @var
     */
    protected $cache;

    /**
     * Construct the Cache Repository
     *
     * @return  void
     */
    public function __construct()
    {
        //
    }

    /**
     * {@inheritdoc}
     */
    public function shouldLoadRelationships()
    {
        return $this->request->has('embed') ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function requestedRelationships()
    {
        return $this->shouldLoadRelationships() ? (explode(',', $this->request->input('embed'))) : [];
    }
}
