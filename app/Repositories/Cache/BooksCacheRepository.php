<?php

namespace App\Repositories\Cache;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Repository;

// Contracts
use App\Repositories\Contracts\BooksRepositoryContract;

class BooksCacheRepository extends BaseCacheRepository implements BooksRepositoryContract
{
    /**
     * Construct the Cache Repository
     *
     * @param Request $request
     * @param BooksRepositoryContract $repository
     * @param Repository $cache
     * @return  void
     */
    public function __construct(Request $request, BooksRepositoryContract $repository, Repository $cache)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->all();
        }

        // Pull data out of cache, if it exists...
        return $this->cache->tags(['books:all'])->remember('books:all:page_' . $this->request->input('page', 1), config('domain-logic.cache.key_tty'), function() {

            // If cache has expired, grab out of the database
            return $this->repository->all();

        });
    }

    /**
     * {@inheritdoc}
     */
    public function search($query)
    {
        return $this->repository->search($query);
    }

    /**
     * {@inheritdoc}
     */
    public function get(int $id)
    {
        if ($this->repository->shouldLoadRelationships() === true) {
            return $this->repository->get($id);
        }

        // Pull data out of cache, if it exists...
        return $this->cache->remember('books:' . $id, config('domain-logic.cache.key_tty'), function() use($id) {

            // If cache has expired, grab out of the database
            return $this->repository->get($id);

        });
    }

    /**
     * {@inheritdoc}
     */
    public function exists(int $id)
    {
        return $this->repository->exists($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $this->cache->tags(['books:all'])->flush();

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data)
    {
        $this->cache->tags(['books:all'])->flush();
        $this->cache->forget('books:' . $id);

        return $this->repository->update($id, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(int $id)
    {
        $this->cache->tags(['books:all'])->flush();
        $this->cache->forget('books:' . $id);

        return $this->repository->delete($id);
    }
}
