<?php

namespace App\Repositories\Contracts;

interface AuthorsRepositoryContract
{
    /**
     * Get all authors.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Search for an author.
     *
     * @param string $query
     * @return \App\Models\Authors
     */
    public function search($query);

    /**
     * Get a author.
     *
     * @param int $id
     * @return \App\Models\Authors
     */
    public function get(int $id);

    /**
     * Determine if an author exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a author with the given data.
     *
     * @param  array $data
     * @return \App\Models\Authors
     */
    public function create(array $data);

    /**
     * Update a author with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Authors
     */
    public function update(int $id, array $data);

    /**
     * Deletes a author.
     *
     * @param int
     * @return \App\Models\Authors
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}