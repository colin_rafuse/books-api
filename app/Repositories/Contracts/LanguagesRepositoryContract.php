<?php

namespace App\Repositories\Contracts;

interface LanguagesRepositoryContract
{
    /**
     * Get all languages.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Get a language.
     *
     * @param int $id
     * @return \App\Models\Languages
     */
    public function get(int $id);

    /**
     * Determine if a language exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a language with the given data.
     *
     * @param  array $data
     * @return \App\Models\Languages
     */
    public function create(array $data);

    /**
     * Update a language with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Languages
     */
    public function update(int $id, array $data);

    /**
     * Deletes a language.
     *
     * @param int
     * @return \App\Models\Languages
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}