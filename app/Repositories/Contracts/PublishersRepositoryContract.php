<?php

namespace App\Repositories\Contracts;

interface PublishersRepositoryContract
{
    /**
     * Get all publishers.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Get a publisher.
     *
     * @param int $id
     * @return \App\Models\Publishers
     */
    public function get(int $id);

    /**
     * Determine if a publisher exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a publisher with the given data.
     *
     * @param  array $data
     * @return \App\Models\Publishers
     */
    public function create(array $data);

    /**
     * Update a publisher with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Publishers
     */
    public function update(int $id, array $data);

    /**
     * Deletes a publisher.
     *
     * @param int
     * @return \App\Models\Publishers
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}