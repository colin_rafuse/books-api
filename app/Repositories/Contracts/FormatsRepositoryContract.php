<?php

namespace App\Repositories\Contracts;

interface FormatsRepositoryContract
{
    /**
     * Get all formats.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Get a format.
     *
     * @param int $id
     * @return \App\Models\Formats
     */
    public function get(int $id);

    /**
     * Determine if a format exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a format with the given data.
     *
     * @param  array $data
     * @return \App\Models\Formats
     */
    public function create(array $data);

    /**
     * Update a format with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Formats
     */
    public function update(int $id, array $data);

    /**
     * Deletes a format.
     *
     * @param int
     * @return \App\Models\Formats
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}