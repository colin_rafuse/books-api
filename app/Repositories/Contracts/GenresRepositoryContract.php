<?php

namespace App\Repositories\Contracts;

interface GenresRepositoryContract
{
    /**
     * Get all genres.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Get a genre.
     *
     * @param int $id
     * @return \App\Models\Genres
     */
    public function get(int $id);

    /**
     * Determine if a genre exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a genre with the given data.
     *
     * @param  array $data
     * @return \App\Models\Genres
     */
    public function create(array $data);

    /**
     * Update a genre with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Genres
     */
    public function update(int $id, array $data);

    /**
     * Deletes a genre.
     *
     * @param int
     * @return \App\Models\Genres
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}