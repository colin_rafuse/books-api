<?php

namespace App\Repositories\Contracts;

interface TypesRepositoryContract
{
    /**
     * Get all types.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Get a type.
     *
     * @param int $id
     * @return \App\Models\Types
     */
    public function get(int $id);

    /**
     * Determine if a type exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a type with the given data.
     *
     * @param  array $data
     * @return \App\Models\Types
     */
    public function create(array $data);

    /**
     * Update a type with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Types
     */
    public function update(int $id, array $data);

    /**
     * Deletes a type.
     *
     * @param int
     * @return \App\Models\Types
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}
