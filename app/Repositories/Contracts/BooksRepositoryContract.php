<?php

namespace App\Repositories\Contracts;

interface BooksRepositoryContract
{
    /**
     * Get all books.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Search for a book.
     *
     * @param string $query
     * @return \App\Models\Books
     */
    public function search($query);

    /**
     * Get a book.
     *
     * @param int $id
     * @return \App\Models\Books
     */
    public function get(int $id);

    /**
     * Determine if a book exists.
     *
     * @param int $id
     * @return boolean
     */
    public function exists(int $id);

    /**
     * Create a book with the given data.
     *
     * @param  array $data
     * @return \App\Models\Books
     */
    public function create(array $data);

    /**
     * Update a book with the given data.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Books
     */
    public function update(int $id, array $data);

    /**
     * Deletes a book.
     *
     * @param int
     * @return \App\Models\Books
     */
    public function delete(int $id);

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function shouldLoadRelationships();

    /**
     * Should we try to Load Relationships?
     *
     * @return boolean
     */
    public function requestedRelationships();
}