<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;

// Repositories
use App\Repositories\Cache\BooksCacheRepository;
use App\Repositories\Eloquent\BooksEloquentRepository;

use App\Repositories\Cache\AuthorsCacheRepository;
use App\Repositories\Eloquent\AuthorsEloquentRepository;

use App\Repositories\Cache\PublishersCacheRepository;
use App\Repositories\Eloquent\PublishersEloquentRepository;

use App\Repositories\Cache\TypesCacheRepository;
use App\Repositories\Eloquent\TypesEloquentRepository;

use App\Repositories\Cache\GenresCacheRepository;
use App\Repositories\Eloquent\GenresEloquentRepository;

use App\Repositories\Cache\FormatsCacheRepository;
use App\Repositories\Eloquent\FormatsEloquentRepository;

use App\Repositories\Cache\LanguagesCacheRepository;
use App\Repositories\Eloquent\LanguagesEloquentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Return a single implementation of the contract
        //$this->app->bind(
        //    'App\Repositories\Contracts\ProvincesRepositoryContract',
        //    'App\Repositories\Eloquent\ProvincesEloquentRepository'
        //);

        $request = request();

        // Books
        $this->app->singleton('App\Repositories\Contracts\BooksRepositoryContract', function() use($request) {
            $eloquentRepository = new BooksEloquentRepository($request);
            $cachingRepository = new BooksCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Authors
        $this->app->singleton('App\Repositories\Contracts\AuthorsRepositoryContract', function() use($request) {
            $eloquentRepository = new AuthorsEloquentRepository($request);
            $cachingRepository = new AuthorsCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Publishers
        $this->app->singleton('App\Repositories\Contracts\PublishersRepositoryContract', function() use($request) {
            $eloquentRepository = new PublishersEloquentRepository($request);
            $cachingRepository = new PublishersCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Types
        $this->app->singleton('App\Repositories\Contracts\TypesRepositoryContract', function() use($request) {
            $eloquentRepository = new TypesEloquentRepository($request);
            $cachingRepository = new TypesCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Genres
        $this->app->singleton('App\Repositories\Contracts\GenresRepositoryContract', function() use($request) {
            $eloquentRepository = new GenresEloquentRepository($request);
            $cachingRepository = new GenresCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Formats
        $this->app->singleton('App\Repositories\Contracts\FormatsRepositoryContract', function() use($request) {
            $eloquentRepository = new FormatsEloquentRepository($request);
            $cachingRepository = new FormatsCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });

        // Book Languages
        $this->app->singleton('App\Repositories\Contracts\LanguagesRepositoryContract', function() use($request) {
            $eloquentRepository = new LanguagesEloquentRepository($request);
            $cachingRepository = new LanguagesCacheRepository($request, $eloquentRepository, Cache::driver());
            return $cachingRepository;
        });
    }
}
