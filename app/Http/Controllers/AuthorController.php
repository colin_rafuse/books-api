<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\AuthorsRepositoryContract;

// Resources
use App\Http\Resources\AuthorsResource;
use App\Http\Resources\AuthorsResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class AuthorController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Author Repository Contract class instance.
     *
     * @var
     */
    protected $authors;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthorsRepositoryContract $authors)
    {
        $this->authors = $authors;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new AuthorsResourceCollection($this->authors->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Search the model and return an index of the resource.
     *
     * @param \Illuminate\Http\Request
     * @param string $query
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $query)
    {
        return (new AuthorsResourceCollection($this->authors->search($query)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        return (new AuthorsResource($this->authors->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->authors->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new AuthorsResource($this->authors->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($this->authors->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new AuthorsResource($this->authors->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->authors->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->authors->delete($id);

        return response()->json([], 204);
    }
}
