<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\TypesRepositoryContract;

// Resources
use App\Http\Resources\TypesResource;
use App\Http\Resources\TypesResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class TypeController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Type Repository Contract class instance.
     *
     * @var
     */
    protected $types;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TypesRepositoryContract $types)
    {
        $this->types = $types;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new TypesResourceCollection($this->types->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        return (new TypesResource($this->types->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->types->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new TypesResource($this->types->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($this->types->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new TypesResource($this->types->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->types->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->types->delete($id);

        return response()->json([], 204);
    }
}
