<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\FormatsRepositoryContract;

// Resources
use App\Http\Resources\FormatsResource;
use App\Http\Resources\FormatsResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class FormatController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Format Repository Contract class instance.
     *
     * @var
     */
    protected $formats;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FormatsRepositoryContract $formats)
    {
        $this->formats = $formats;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new FormatsResourceCollection($this->formats->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        return (new FormatsResource($this->formats->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->formats->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new FormatsResource($this->formats->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($this->formats->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new FormatsResource($this->formats->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->formats->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->formats->delete($id);

        return response()->json([], 204);
    }
}
