<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\LanguagesRepositoryContract;

// Resources
use App\Http\Resources\LanguagesResource;
use App\Http\Resources\LanguagesResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class LanguageController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Language Repository Contract class instance.
     *
     * @var
     */
    protected $languages;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LanguagesRepositoryContract $languages)
    {
        $this->languages = $languages;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new LanguagesResourceCollection($this->languages->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'abbreviation' => 'required',
        ]);

        return (new LanguagesResource($this->languages->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->languages->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new LanguagesResource($this->languages->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'abbreviation' => 'required',
        ]);

        if ($this->languages->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new LanguagesResource($this->languages->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->languages->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->languages->delete($id);

        return response()->json([], 204);
    }
}
