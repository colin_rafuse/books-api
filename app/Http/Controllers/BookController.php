<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\BooksRepositoryContract;

// Resources
use App\Http\Resources\BooksResource;
use App\Http\Resources\BooksResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class BookController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Book Repository Contract class instance.
     *
     * @var
     */
    protected $books;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BooksRepositoryContract $books)
    {
        $this->books = $books;
    }

    /**
     * Search the model and return an index of the resource.
     *
     * @param \Illuminate\Http\Request
     * @param string $query
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $query)
    {
        return (new BooksResourceCollection($this->books->search($query)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Display an index of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new BooksResourceCollection($this->books->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'title' => 'required',
            'description' => 'required',
            'author_id' => 'required|array',
            'publisher_id' => 'required',
            'genre_id' => 'required',
            'type_id' => 'required',
            'format_id' => 'required',
            'language_id' => 'required',
            'pages' => 'required',
            'published_at' => 'sometimes',
            'isbn_ten' => 'required',
            'isbn_thirteen' => 'required'
        ]);

        return (new BooksResource($this->books->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->books->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new BooksResource($this->books->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'title' => 'required',
            'description' => 'required',
            'author_id' => 'required|array',
            'publisher_id' => 'required',
            'genre_id' => 'required',
            'type_id' => 'required',
            'format_id' => 'required',
            'language_id' => 'required',
            'pages' => 'required',
            'published_at' => 'sometimes',
            'isbn_ten' => 'required',
            'isbn_thirteen' => 'required',
        ]);

        if ($this->books->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new BooksResource($this->books->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->books->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->books->delete($id);

        return response()->json([], 204);
    }
}
