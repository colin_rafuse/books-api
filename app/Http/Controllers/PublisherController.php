<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\PublishersRepositoryContract;

// Resources
use App\Http\Resources\PublishersResource;
use App\Http\Resources\PublishersResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class PublisherController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Publisher Repository Contract class instance.
     *
     * @var
     */
    protected $publishers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PublishersRepositoryContract $publishers)
    {
        $this->publishers = $publishers;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new PublishersResourceCollection($this->publishers->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        return (new PublishersResource($this->publishers->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->publishers->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new PublishersResource($this->publishers->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($this->publishers->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new PublishersResource($this->publishers->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->publishers->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->publishers->delete($id);

        return response()->json([], 204);
    }
}
