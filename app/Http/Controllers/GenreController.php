<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Repositories
use App\Repositories\Contracts\GenresRepositoryContract;

// Resources
use App\Http\Resources\GenresResource;
use App\Http\Resources\GenresResourceCollection;

// Exceptions
use App\Exceptions\ItemNotFoundException;

class GenreController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * The Genre Repository Contract class instance.
     *
     * @var
     */
    protected $genres;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GenresRepositoryContract $genres)
    {
        $this->genres = $genres;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return (new GenresResourceCollection($this->genres->all()))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->processValidation($request, [
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        return (new GenresResource($this->genres->create($request->all())))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->genres->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new GenresResource($this->genres->get($id)))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
            'slug' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($this->genres->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        return (new GenresResource($this->genres->update($id, $request->all())))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->processValidation($request, [
            'id' => 'required|integer',
        ]);

        if ($this->genres->exists($id) === false) {
            throw new ItemNotFoundException($id);
        }

        $this->genres->delete($id);

        return response()->json([], 204);
    }
}
