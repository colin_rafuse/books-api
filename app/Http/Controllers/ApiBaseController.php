<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

// Exceptions
use App\Exceptions\ValidationFailureException;

class ApiBaseController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Construct the Api Base controller class.
     *
     * @return void
     */
    public function __construct()
    {
      //
    }

    /**
     * Process validation rules. Throw an exception if the validator fails.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param array $rules
     * @return void
     */
    public function processValidation(Request $request, $rules)
    {
        // Combine the Request Payload and the Request Route Parameters
        // so we can validate all incoming Parameters, whereas $request->all() only
        // provides payload and query string parameters
        $request_data = array_merge($request->all(), $request->route()->parameters());

        $validator = Validator::make($request_data, $rules);

        if ($validator->fails()) {
            throw new ValidationFailureException($validator);
        }
    }
}
