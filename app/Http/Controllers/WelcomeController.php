<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends \App\Http\Controllers\ApiBaseController
{
    /**
     * Show the API Welcome Response.
     *
     * @return Response
     */
    public function __invoke()
    {
        $welcome_data = [
            'A RESTful Books API' => [
                'BitBucket' => 'https://bitbucket.org/colin_rafuse/books-api/',
                'Postman' => 'https://documenter.getpostman.com/view/1670890/books-api/RWEduLpe',
            ]
        ];
        return response()->json($welcome_data, 200);
    }
}
