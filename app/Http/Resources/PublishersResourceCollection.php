<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

// Traits
use App\Traits\ResourceResponse;

class PublishersResourceCollection extends ResourceCollection
{
    use ResourceResponse;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->additional($this->provideMetaData($request));

        return parent::toArray($request);
    }
}
