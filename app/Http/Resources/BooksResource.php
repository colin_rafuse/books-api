<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

// Resources
use App\Http\Resources\PublishersResource;
use App\Http\Resources\GenresResource;
use App\Http\Resources\TypesResource;
use App\Http\Resources\FormatsResource;
use App\Http\Resources\LanguagesResource;

// Traits
use App\Traits\ResourceResponse;

class BooksResource extends Resource
{
    use ResourceResponse;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->additional($this->provideMetaData($request));

        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'description' => $this->description,
            'authors' => $this->whenLoaded('authors'),
            'publisher_id' => $this->publisher_id,
            'publisher' => $this->whenLoaded('publisher'),
            'genre_id' => $this->genre_id,
            'genre' => $this->whenLoaded('genre'),
            'type_id' => $this->type_id,
            'type' => $this->whenLoaded('type'),
            'format_id' => $this->format_id,
            'format' => $this->whenLoaded('format'),
            'language_id' => $this->language_id,
            'language' => $this->whenLoaded('language'),
            'pages' => $this->pages,
            'published_at' => $this->published_at,
            'isbn_ten' => $this->isbn_ten,
            'isbn_thirteen' => $this->isbn_thirteen,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
