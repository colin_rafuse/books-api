<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

// Resources
use App\Http\Resources\BooksResource;

// Traits
use App\Traits\ResourceResponse;

class GenresResource extends Resource
{
    use ResourceResponse;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->additional($this->provideMetaData($request));

        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'books' => $this->whenLoaded('books'),
        ];
    }
}
