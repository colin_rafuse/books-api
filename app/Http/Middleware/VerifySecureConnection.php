<?php

namespace App\Http\Middleware;

use Closure;

// Exceptions
use App\Exceptions\InsecureConnectionException;

class VerifySecureConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->secure()) {
            throw new InsecureConnectionException();
        }

        return $next($request);
    }
}
