<?php

if (!function_exists('enforceEmptyObject')) {

    /**
     * Return the Array as provided if not empty. If the array is empty, return a new Std Class object.
     * This will ensure the JSON response will include an empty object instead of an empty array. This adds
     * consistent behaviour to the responses so our clients are looking for an object OR an empty array.
     *
     * Default Laravel Behaviour:
     *
     *  PHP: 'sample_array' => ['one' => 'test', 'two' => 'test'];
     *  JSON Response: 'sample_array' => {"one": "test", "two": "test"}
     *
     *  PHP: 'sample_array' => [];
     *  JSON Response: 'sample_array' => []
     *
     * With enforceEmptyObject() Function:
     *
     *  PHP: 'sample_array' => ['one' => 'test', 'two' => 'test'];
     *  JSON Response: 'sample_array' => {"one": "test", "two": "test"}
     *
     *  PHP: 'sample_array' => [];
     *  JSON Response: 'sample_array' => {}
     *
     * @param  array  $array
     * @return array|object
     */
    function enforceEmptyObject($array)
    {
        return  $array ?: new \StdClass;
    }

}
