<?php

namespace App\Exceptions;

use Exception;

class InsecureConnectionException extends Exception
{
    /**
     * Exception instance.
     *
     * @var \Exception
     */
    protected $exception;

    /**
     * Exception HTTP Status Code.
     *
     * @var integer
     */
    protected $status_code = 405;

    /**
     * Exception Type.
     *
     * @var string
     */
    protected $type = 'insecure-connection';

    /**
     * Construct the Exception class.
     *
     * @param string $message
     * @return void
     */
    public function __construct($message = "Insecure Connection")
    {
        $this->exception = new Exception($message);
    }

    /**
     * Report the exception.
     *
     * @return void
     */
     public function report()
     {
        //
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $exception_response = [
            'exception' => [
                'name' => $this->type,
                'status_code' => $this->status_code,
                'message' => $this->exception->getMessage(),
            ]
        ];

        return response($exception_response, $this->status_code);
    }
}
