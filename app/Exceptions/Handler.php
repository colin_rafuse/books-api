<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        Log::debug('WHOOPS' . $exception->getMessage());
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $status_code = 500;
        $exception_response = [
            'exception' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'stack_trace' => $exception->getTrace(),
            ]
        ];

        if ($exception instanceof \App\Exceptions\ItemNotFoundException) {
            return $exception->render($request, $exception);
        }

        if ($exception instanceof \App\Exceptions\ValidationFailureException) {
            return $exception->render($request, $exception);
        }

        if ($exception instanceof \App\Exceptions\InsecureConnectionException) {
            return $exception->render($request, $exception);
        }

        if ($exception instanceof \Illuminate\Database\Eloquent\RelationNotFoundException) {
            $status_code = 422;
            $exception_response = [
                'exception' => [
                    'name' => 'relationship-not-found',
                    'status' => '422',
                    'message' => $exception->getMessage(),
                ],
            ];
        }

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $status_code = 404;
            $exception_response = [
                'exception' => [
                    'name' => 'route-not-found',
                    'status' => '404',
                    'message' => 'Route not found',
                ],
            ];
        }

        return response()->json($exception_response, $status_code);
    }
}
