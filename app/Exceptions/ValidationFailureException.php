<?php

namespace App\Exceptions;

use Exception;

class ValidationFailureException extends Exception
{
    /**
     * Validator instance.
     *
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * Exception HTTP Status Code.
     *
     * @var integer
     */
    protected $status_code = 422;

    /**
     * Exception Type.
     *
     * @var string
     */
    protected $type = 'validation-failure';

    /**
     * Construct the Exception class.
     *
     * @param \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function __construct(\Illuminate\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Report the exception.
     *
     * @return void
     */
     public function report()
     {
        //
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $exception_response = [
            'exception' => [
                'name' => $this->type,
                'status_code' => $this->status_code,
                'message' => $this->validator->errors(),
            ]
        ];

        return response($exception_response, $this->status_code);
    }
}
