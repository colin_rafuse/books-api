<?php

namespace App\Exceptions;

use Exception;

class ItemNotFoundException extends Exception
{
    /**
     * Exception instance.
     *
     * @var \Exception
     */
    protected $exception;

    /**
     * Exception HTTP Status Code.
     *
     * @var integer
     */
    protected $status_code = 404;

    /**
     * Exception Type.
     *
     * @var string
     */
    protected $type = 'item-not-found';

    /**
     * Construct the Exception class.
     *
     * @param integer $id
     * @param string $message
     * @return void
     */
    public function __construct($id, $message = null)
    {
        if (is_null($message)) {
            $message = 'Item with id ' . $id . ' Not Found.';
        }

        $this->exception = new Exception($message);
    }

    /**
     * Report the exception.
     *
     * @return void
     */
     public function report()
     {
        //
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        $exception_response = [
            'exception' => [
                'name' => $this->type,
                'status_code' => $this->status_code,
                'message' => $this->exception->getMessage(),
            ]
        ];

        return response($exception_response, $this->status_code);
    }
}
