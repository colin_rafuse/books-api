<?php

namespace App\Traits;

use Carbon\Carbon;

trait ResourceResponse
{
    /**
     * Provide the Resource meta data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function provideMetaData($request)
    {
        return [
            'meta' => [
                'request_id' => uniqid(),
                'request_method' => $request->method(),
                'request_url' => url()->current(),
                'request_full_url' => url()->full(),
                'request_route_parameters' => enforceEmptyObject($request->route()->parameters()),
                'request_query_string_parameters' => enforceEmptyObject($request->query()),
                'request_time_start_ms' => constant('LARAVEL_START'),
                'request_time_end_ms' => microtime(true),
                'request_time_delta_ms' => round(microtime(true) - constant('LARAVEL_START'),3) * 10000,
            ]
        ];
    }
}
