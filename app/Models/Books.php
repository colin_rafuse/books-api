<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Books extends BaseModel
{
    use SoftDeletes;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * The primary key for table associated with the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'title' => 'string',
        'description' => 'string',
        'publisher_id' => 'integer',
        'genre_id' => 'integer',
        'type_id' => 'integer',
        'format_id' => 'integer',
        'language_id' => 'integer',
        'pages' => 'integer',
        'published_at' => 'datetime',
        'isbn_ten' => 'string',
        'isbn_thirteen' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Book Relationships
     */

        /**
         * Get the author records associated with the book.
         */
        public function authors()
        {
            return $this->belongsToMany('App\Models\Authors', 'book_author', 'book_id', 'author_id')->withTimestamps();
        }

        /**
         * Get the publisher record associated with the book.
         */
        public function publisher()
        {
            return $this->belongsTo('App\Models\Publishers', 'publisher_id', 'id');
        }

        /**
         * Get the genre record associated with the book.
         */
        public function genre()
        {
            return $this->belongsTo('App\Models\Genres', 'genre_id', 'id');
        }

        /**
         * Get the type record associated with the book.
         */
        public function type()
        {
            return $this->belongsTo('App\Models\Types', 'type_id', 'id');
        }

        /**
         * Get the format record associated with the book.
         */
        public function format()
        {
            return $this->belongsTo('App\Models\Formats', 'format_id', 'id');
        }

        /**
         * Get the language record associated with the book.
         */
        public function language()
        {
            return $this->belongsTo('App\Models\Languages', 'language_id', 'id');
        }
}
