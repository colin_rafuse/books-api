<?php

use Illuminate\Database\Seeder;

use App\Models\Books;
use App\Models\Authors;
use App\Models\Publishers;
use App\Models\Formats;
use App\Models\Genres;
use App\Models\Types;
use App\Models\Languages;

class PopulateBooksDBStaticTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create the static Book records
        Books::firstOrCreate([
            'slug' => 'the-outsider-a-novel',
            'title' => 'The Outsider: A Novel',
            'description' => "An unspeakable crime. A confounding investigation. At a time when the King brand has never been stronger, he has delivered one of his most unsettling and compulsively readable stories.",
            'publisher_id' => 1,
            'genre_id' => 13,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 576,
            'published_at' => '2018-05-28',
            'isbn_ten' => '1501180983',
            'isbn_thirteen' => '9781501180989',
        ]);

        Books::firstOrCreate([
            'slug' => 'warlight-a-novel',
            'title' => 'Warlight: A Novel',
            'description' => "An unspeakable crime. A confounding investigation. At a time when the King brand has never been stronger, he has delivered one of his most unsettling and compulsively readable stories.",
            'publisher_id' => 2,
            'genre_id' => 6,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 304,
            'published_at' => '2018-05-08',
            'isbn_ten' => '077107378X',
            'isbn_thirteen' => '9780771073786',
        ]);

        Books::firstOrCreate([
            'slug' => 'fire-blood-300-years-before',
            'title' => 'Fire & Blood: 300 Years Before A Game Of Thrones (a Targaryen History)',
            'description' => "The thrilling history of the Targaryens comes to life in this masterly work by the author of A Song of Ice and Fire, the inspiration for HBO’s Game of Thrones.",
            'publisher_id' => 3,
            'genre_id' => 6,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 736,
            'published_at' => '2018-11-20',
            'isbn_ten' => '152479628X',
            'isbn_thirteen' => '9781524796280',
        ]);

        Books::firstOrCreate([
            'slug' => 'some-trick-thirteen-stories',
            'title' => 'Some Trick: Thirteen Stories',
            'description' => "For sheer unpredictable brilliance, Gogol may come to mind, but no author alive today takes a reader as far as Helen DeWitt into the funniest, most yonder dimensions of possibility. Her jumping-off points might be statistics, romance, the art world’s piranha tank, games of chance and games of skill, the travails of publishing, or success.",
            'publisher_id' => 4,
            'genre_id' => 7,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 224,
            'published_at' => '2018-06-06',
            'isbn_ten' => '0811227820',
            'isbn_thirteen' => '9780811227827',
        ]);

        Books::firstOrCreate([
            'slug' => 'how-to-grill-everything',
            'title' => 'How To Grill Everything: Simple Recipes For Great Flame-cooked Food',
            'description' => "The ultimate grilling guide and the latest in Mark Bittman's acclaimed How to Cook Everything series.",
            'publisher_id' => 5,
            'genre_id' => 3,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 576,
            'published_at' => '2018-04-24',
            'isbn_ten' => '0544790308',
            'isbn_thirteen' => '9780544790308',
        ]);

        Books::firstOrCreate([
            'slug' => 'a-chronology-of-art',
            'title' => 'A Chronology Of Art',
            'description' => "Most surveys of the history of art are divided into historic periods, artistic schools, and movements. In reality, movements and artists’ careers overlap and intertwine, reacting to events in the world around them. By prioritizing a purely chronological approach, A Chronology of Art illuminates these relationships from a fresh perspective and places the developments of the art world into context with one another.",
            'publisher_id' => 6,
            'genre_id' => 1,
            'type_id' => 3,
            'format_id' => 1,
            'language_id' => 1,
            'pages' => 288,
            'published_at' => '2018-02-06',
            'isbn_ten' => '0500239819',
            'isbn_thirteen' => '9780500239810',
        ]);

        Books::firstOrCreate([
            'slug' => 'full-dark-no-stars',
            'title' => 'Full Dark, No Stars',
            'description' => "From the #1 New York Times bestselling author Stephen King, four “disturbing, fascinating” (The Washington Post) novellas—including the story “1922,” a Netflix original film—that explore the dark side of human nature.",
            'publisher_id' => 1,
            'genre_id' => 13,
            'type_id' => 3,
            'format_id' => 2,
            'language_id' => 1,
            'pages' => 384,
            'published_at' => '2018-06-12',
            'isbn_ten' => '1501197940',
            'isbn_thirteen' => '9781501197949',
        ]);

        // Create the static Author records
        Authors::firstOrCreate(['slug' => 'stephen-king', 'name' => 'Stephen King', 'description' => "Stephen King was born in Portland, Maine, on September 21, 1947. After graduating with a Bachelor's degree in English from the University of Maine at Orono in 1970, he became a teacher. His spare time was spent writing short stories and novels. King's first novel would never have been published if not for his wife. She removed the first few chapters from the garbage after King had thrown them away in frustration. Three months later, he received a $2,500 advance from Doubleday Publishing for the book that went on to sell a modest 13,000 hardcover copies. That book, Carrie, was about a girl with telekinetic powers who is tormented by bullies at school. She uses her power, in turn, to torment and eventually destroy her mean-spirited classmates. When United Artists released the film version in 1976, it was a critical and commercial success. The paperback version of the book, released after the movie, went on to sell more than two-and-a-half million copies. Many of King's other horror novels have been adapted into movies, including The Shining, Firestarter, Pet Semetary, Cujo, Misery, The Stand, and The Tommyknockers. Under the pseudonym Richard Bachman, King has written the books The Running Man, The Regulators, Thinner, The Long Walk, Roadwork, and Rage. He is number 2 on the Hollywood Reporter's '25 Most Powerful Authors' 2016 list. King is one of the world's most successful writers, with more than 100 million copies of his works in print. Many of his books have been translated into foreign languages, and he writes new books at a rate of about one per year. In 2003, he received the National Book Foundation Medal for Distinguished Contribution to American Letters. In 2012 his title, The Wind Through the Keyhole made The New York Times Best Seller List. King's title's Mr. Mercedes and Revival made The New York Times Best Seller List in 2014. He won the Edgar Allan Poe Award in 2015 for Best Novel with Mr. Mercedes. King's title Finders Keepers made the New York Times bestseller list in 2015."]);
        Authors::firstOrCreate(['slug' => 'michael-ondaatje', 'name' => 'Michael Ondaatje', 'description' => "MICHAEL ONDAATJE is the author of several award-winning novels, as well as a memoir, a nonfiction book on film, and several books of poetry. Among other accolades, his novel The English Patient won the Booker Prize, and Anil's Ghost won the Irish Times International Fiction Prize, the Giller prize, and the Prix Médicis. Born in Sri Lanka, Michael Ondaatje lives in Toronto, Canada."]);
        Authors::firstOrCreate(['slug' => 'george-r-r-martin', 'name' => 'George R. R. Martin', 'description' => "George R. R. Martin is the #1 New York Times bestselling author of many novels, including those of the acclaimed series A Song of Ice and Fire—A Game of Thrones, A Clash of Kings, A Storm of Swords, A Feast for Crows, and A Dance with Dragons—as well as Tuf Voyaging, Fevre Dream, The Armageddon Rag, Dying of the Light, Windhaven (with Lisa Tuttle), and Dreamsongs Volumes I and II. He is also the creator of The Lands of Ice and Fire, a collection of maps featuring original artwork from illustrator and cartographer Jonathan Roberts, and The World of Ice & Fire, with Elio M. García, Jr., and Linda Antonsson. As a writer-producer, he has worked on The Twilight Zone, Beauty and the Beast, and various feature films and pilots that were never made. He lives with the lovely Parris in Santa Fe, New Mexico.   Doug Wheatley is a comic book artist, concept designer, and illustrator who has worked on such properties and characters as Star Wars, Aliens, Superman, The Incredible Hulk, and Conan the Barbarian, to name just a few. Wheatley was the artist on the comic book adaptation of the film Star Wars: Episode III: Revenge of the Sith and contributed illustrations to The World of Ice & Fire."]);
        Authors::firstOrCreate(['slug' => 'helen-dewitt', 'name' => 'Helen Dewitt', 'description' => "Author of The Last Samurai and Lightning Rods, “Helen Dewitt knows, in descending order of proficiency, Latin, ancient Greek, French, German, Spanish, Italian, Portuguese, Dutch, Danish, Norwegian, Swedish, Arabic, Hebrew, and Japanese: 'The self is a set of linguistic patterns,' she said. 'Reading and speaking in another language is like stepping into an alternate history of yourself where all the bad connotations are gone' (New York Magazine)."]);
        Authors::firstOrCreate(['slug' => 'mark-bittman', 'name' => 'Mark Bittman', 'description' => "MARK BITTMAN is the author of 20 acclaimed books, including the How to Cook Everything series, the award-winning Food Matters, and t he New York Times number-one bestseller, VB6: Eat Vegan Before 6:00 . For more than two decades his popular and compelling stories appeared in the Times, where he was ultimately the lead food writer for the Sunday magazine and became the country's first food-focused Op-Ed columnist for a major news publication."]);
        Authors::firstOrCreate(['slug' => 'iain-zaczek', 'name' => 'Iain Zaczek', 'description' => "Iain Zaczek was educated at Wadham College, Oxford, and the Courtauld Institute of Art. He specializes in books about art and design, and his many books include The Impressionists, Essential Art Deco, Celtic Art and Design, and The Art of Illuminated Manuscripts."]);

        // Create the static Publisher records
        Publishers::firstOrCreate(['slug' => 'scribner', 'name' => 'Scribner', 'description' => 'Scribner']);
        Publishers::firstOrCreate(['slug' => 'mcclelland-Stewart', 'name' => 'McClelland & Stewart', 'description' => 'McClelland & Stewart']);
        Publishers::firstOrCreate(['slug' => 'random-house-publishing-group', 'name' => 'Random House Publishing Group', 'description' => 'Random House Publishing Group']);
        Publishers::firstOrCreate(['slug' => 'ww-norton', 'name' => 'WW Norton', 'description' => 'WW Norton']);
        Publishers::firstOrCreate(['slug' => 'houghton-mifflin-harcourt', 'name' => 'Houghton Mifflin Harcourt', 'description' => 'Houghton Mifflin Harcourt']);

        // Create the static Types records
        Types::firstOrCreate(['slug' => 'teen', 'name' => 'Teen', 'description' => 'Books for Teens']);
        Types::firstOrCreate(['slug' => 'kids', 'name' => 'Kids', 'description' => 'Books for Kids']);
        Types::firstOrCreate(['slug' => 'adult', 'name' => 'Adult', 'description' => 'Books for Adults']);

        // Create the static Genres records
        Genres::firstOrCreate(['slug' => 'art', 'name' => 'Art', 'description' => 'Art Books']);
        Genres::firstOrCreate(['slug' => 'biography', 'name' => 'Biogrpahy', 'description' => 'Biographies & Memoirs']);
        Genres::firstOrCreate(['slug' => 'cookbooks', 'name' => 'Cookbooks', 'description' => 'Cooking Books']);
        Genres::firstOrCreate(['slug' => 'entertainment', 'name' => 'Entertainment', 'description' => 'Entertainment Books']);
        Genres::firstOrCreate(['slug' => 'faith', 'name' => 'Faith', 'description' => 'Faith Books']);
        Genres::firstOrCreate(['slug' => 'fantasy', 'name' => 'Fantasy', 'description' => 'Fantasy Books']);
        Genres::firstOrCreate(['slug' => 'fiction', 'name' => 'Fiction', 'description' => 'Fiction Books']);
        Genres::firstOrCreate(['slug' => 'graphic-novels', 'name' => 'Graphic Novels', 'description' => 'Graphic Novel Books']);
        Genres::firstOrCreate(['slug' => 'heatlh', 'name' => 'Health', 'description' => 'Health Books']);
        Genres::firstOrCreate(['slug' => 'history', 'name' => 'History', 'description' => 'History Books']);
        Genres::firstOrCreate(['slug' => 'home-and-garden', 'name' => 'Home & Garden', 'description' => 'Home & Garden Books']);
        Genres::firstOrCreate(['slug' => 'humour', 'name' => 'Humour', 'description' => 'Humour Books']);
        Genres::firstOrCreate(['slug' => 'mystery', 'name' => 'Mystery', 'description' => 'Mystery Books']);
        Genres::firstOrCreate(['slug' => 'science', 'name' => 'Science', 'description' => 'Science Books']);
        Genres::firstOrCreate(['slug' => 'sci-fi', 'name' => 'Sci Fi', 'description' => 'Sci Fi Books']);
        Genres::firstOrCreate(['slug' => 'sports', 'name' => 'Sports', 'description' => 'Sports Books']);
        Genres::firstOrCreate(['slug' => 'travel', 'name' => 'Travel', 'description' => 'Travel Books']);
        Genres::firstOrCreate(['slug' => 'true-crime', 'name' => 'True Crime', 'description' => 'True Crime Books']);

        // Create the static Formats records
        Formats::firstOrCreate(['slug' => 'hardcover', 'name' => 'Hardcover', 'description' => 'Hardcover format']);
        Formats::firstOrCreate(['slug' => 'paperback', 'name' => 'Paperback', 'description' => 'Paperback format']);

        // Create the static Languages records
        Languages::firstOrCreate(['slug' => 'english', 'name' => 'English', 'abbreviation' => 'EN']);
        Languages::firstOrCreate(['slug' => 'french', 'name' => 'French', 'abbreviation' => 'FR']);
    }
}
