<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id')->comment('The books Pk');
            $table->string('slug', 255)->comment('The books slug');
            $table->string('title', 255)->comment('The books title');
            $table->text('description')->comment('The books description');
            $table->unsignedInteger('publisher_id')->comment('The books Publisher');
            $table->unsignedInteger('genre_id')->comment('The books genre  - History, Humour, etc.');
            $table->unsignedInteger('type_id')->comment('The books type - Teen, Adult, etc.');
            $table->unsignedInteger('format_id')->comment('The books format - hardcover, paperback, etc');
            $table->unsignedInteger('language_id')->comment('The book language');
            $table->integer('pages')->nullable()->comment('The book page count');
            $table->date('published_at')->nullable()->comment('The date the book was published');
            $table->string('isbn_ten')->comment('The ISBN-10 number');
            $table->string('isbn_thirteen')->comment('The ISBN-13 number');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('authors', function (Blueprint $table) {
            $table->increments('id')->comment('The authors Pk');
            $table->string('slug', 255)->comment('The authors slug');
            $table->string('name', 255)->comment('The authors name');
            $table->text('description')->comment('The authors description');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('book_author', function (Blueprint $table) {
            $table->increments('id')->comment('The authors Pk');
            $table->integer('book_id')->comment('The book Id');
            $table->integer('author_id')->comment('The author Id');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('publishers', function (Blueprint $table) {
            $table->increments('id')->comment('The publishers Pk');
            $table->string('slug', 255)->comment('The publishers slug');
            $table->string('name', 255)->comment('The publishers name');
            $table->text('description')->comment('The publishers description');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('formats', function (Blueprint $table) {
            $table->increments('id')->comment('The formats Pk');
            $table->string('slug', 255)->comment('The formats slug');
            $table->string('name', 255)->comment('The formats name');
            $table->text('description')->comment('The formats description');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id')->comment('The languages Pk');
            $table->string('slug', 255)->comment('The languages slug');
            $table->string('name', 255)->comment('The language full name');
            $table->string('abbreviation', 2)->comment('The languages abbreviation');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id')->comment('The genres Pk');
            $table->string('slug', 255)->comment('The genres slug');
            $table->string('name', 255)->comment('The genres name');
            $table->string('description', 255)->comment('The genres description');
            $table->timestampsTz();
            $table->softDeletesTz();
        });

        Schema::create('types', function (Blueprint $table) {
            $table->increments('id')->comment('');
            $table->string('slug', 255)->comment('The types slug');
            $table->string('name', 255)->comment('The types name');
            $table->string('description', 255)->comment('The types description');
            $table->timestampsTz();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Schema::dropIfExists('authors');
        Schema::dropIfExists('publishers');
        Schema::dropIfExists('formats');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('types');
        Schema::dropIfExists('genres');
    }
}
