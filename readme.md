## RESTful Books API

A simple RESTul books API built on Laravel 5.5.

## Purpose

The RESTful books API is designed to assist in the building of web and mobile UI. It provides a good mix of data types and model relationships. It also includes the standard RESTful CRUD routes you'd expect for manipulating the data set.

## Authentication

TBA

## Models

The RESTful books API is built on a set of simple data models:

### books

The `books` model contains all data about a given book such as title, description, etc.. Each ISBN has it's own `books` record, so different editions of the same book will have separate `books` records.

### authors

The `authors` model contains all data related to an author such as name and description. A `books` record can have many `authors` and an `authors` record can have many `books`

### publishers

The `publishers` model contains all data for a given publisher such as name and description. A `books` record can only be related to a single `publishers` record.

### genres

The `genres` model contains all data related to a genre i.e. Comedy, Arts, History. A `books` record can only be related to a single `genres` record.

### formats

The `formats` model contains all data related to physical formats of books i.e. paperback, hardcove, etc. A `books` record can only be related to a single `formats` record.

### types

The `types` model contains all data related to logical groupings of books by age demogrpahic i.e. teen, adult, kids, etc. A `books` record can only be related to a single `types` record.

### languages

The `languages` model contains all data related to a books written language i.e. English, French, etc. A `books` record can only be related to a single `languages` record.

## CRUD Routes

There are standard CRUD Routes available for each model:

### search

Search and get an `index` records for the given model. The search criteria is typically compared to the `title` or `name` property of the model.

```php
GET /books/search/The ro
```

```php
GET /books/search/The road to wi
```

```php
GET /books/search/The road to wigan pier
```

### index

Get an `index` of records for the given model

```php
GET /books
```

### store

`store` a new record for a given model

```php
POST /books
```

### show

`show` a single record for a given model

```php
GET /books/1
```

### put

Put and update an existing record for a given model. The `put` request payload validation requires all properties be present for the model being updated. The provided data will overwrite all existing data for the record.

```php
PUT /books/1
```

### delete

Soft `delete` a single record for a given model

```php
DELETE /books/1
```

## Query Parameters

The RESTful books API provides several Query parameters for manipulating a response:

### embed

The `embed` parameter can be used to include a model reated data in the response.

## Pagination

Pagination is returned on all `search` and `index` routes with a default of 10 items per page. The response will include the links and meta data for pagination:

```php
    ...
    
    "links": {
        "first": "https://books-api.colinrafuse.com/books?page=1",
        "last": "https://books-api.colinrafuse.com/books?page=3",
        "prev": null,
        "next": "https://books-api.colinrafuse.com/books?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 3,
        "path": "https://books-api.colinrafuse.com/books",
        "per_page": 10,
        "to": 10,
        "total": 23,
        
        ...
    }
```

## Integrations

TBA

## Postman Documentation

An up to date Postman Collection can be found here: https://documenter.getpostman.com/view/1670890/books-api/RWEduLpe

## License

The RESTful Books API framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
