<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLifecycleLoggerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lifecycle_logger', function (Blueprint $table) {
            $table->increments('id')->comment('The logging Pk');
            $table->string('request_id', 255)->nullable()->default(null)->comment('A unique ID per request via uniqid()');
            $table->string('request_finger_print', 255)->nullable()->default(null)->comment('The Laravel fingerprint of the request');
            $table->string('request_method', 255)->nullable()->default(null)->comment('The Request HTTP Method');
            $table->string('request_root_url', 255)->nullable()->default(null)->comment('The root URL');
            $table->string('request_url', 255)->nullable()->default(null)->comment('The URL - root and route params');
            $table->string('request_full_url', 255)->nullable()->default(null)->comment('The URL - root, route and query params');
            $table->string('request_route_parameters', 255)->nullable()->default(null)->comment('Request route parameters in JSON');
            $table->string('request_query_string_parameters', 255)->nullable()->default(null)->comment('Request query string parameters in JSON');
            $table->text('request_payload')->nullable()->default(null)->comment('Request payload in JSON');
            $table->text('request_headers')->nullable()->default(null)->comment('Request Headers in JSON');
            $table->boolean('request_secure', 255)->nullable()->default(null)->comment('Is the request secure?');
            $table->ipAddress('request_ip')->nullable()->default(null)->comment('Request IP address');
            $table->text('request_user_agent')->nullable()->default(null)->comment('Request User Agent');
            $table->string('request_time_start_ms', 255)->nullable()->default(null)->comment('Request start time in microseconds');
            $table->string('request_time_end_ms', 255)->nullable()->default(null)->comment('Request end time in microseconds');
            $table->string('request_time_delta_ms', 255)->nullable()->default(null)->comment('Request delta in microseconds');
            $table->string('response_status_code', 255)->nullable()->default(null)->comment('Response HTTP status code');
            $table->boolean('response_success_status_code')->nullable()->default(null)->comment('Did the request respond with a successful HTTP status code?');
            $table->boolean('response_exception_status_code')->nullable()->default(null)->comment('Did the request respond with an exception HTTP status code?');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lifecycle_logger');
    }
}
