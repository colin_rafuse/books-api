<?php

namespace TFHInc\LifecycleLogger;

use Illuminate\Support\ServiceProvider;

class LifecycleLoggerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        // Provide the Config File for Publishing
        $this->publishes([
            __DIR__ . '/../config/lifecycle-logger.php' => config_path('lifecycle-logger.php'),
        ], 'config');

        // Publish the Migrations
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Provide Config if not published
        $this->mergeConfigFrom(__DIR__.'/../config/lifecycle-logger.php', 'lifecycle-logger');

        // Provide the LifecycleLogger class as a singleton and provide
        // the LifecycleLogger class with the Request and Response instances
        $this->app->singleton('TFHInc\LifecycleLogger', function () {
            return new TFHInc\LifecycleLogger(
                app(\Illuminate\Http\Request::class),
                app(\Illuminate\Http\Response::class)
            );
        });
    }
}
