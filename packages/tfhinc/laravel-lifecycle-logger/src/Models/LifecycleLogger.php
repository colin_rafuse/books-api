<?php

namespace TFHInc\LifecycleLogger\Models;

use \Illuminate\Database\Eloquent\Model;

class LifecycleLogger extends Model
{
    /**
     * Construct the Lifecycle Logger class.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setConnection(config('lifecycle-logger.db_connection', 'mysql'));
        $this->setTable(config('lifecycle-logger.db_table'));
    }

    /**
     * The primary key for table associated with the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'request_id' => 'string',
        'request_finger_print' => 'string',
        'request_method' => 'string',
        'request_root_url' => 'string',
        'request_url' => 'string',
        'request_full_url' => 'string',
        'request_route_parameters' => 'string',
        'request_query_string_parameters' => 'string',
        'request_payload' => 'string',
        'request_headers' => 'string',
        'request_secure' => 'boolean',
        'request_ip' => 'string',
        'request_user_agent' => 'string',
        'request_time_start_ms' => 'string',
        'request_time_end_ms' => 'string',
        'request_time_delta_ms' => 'string',
        'response_status' => 'string',
        'success_status_code' => 'boolean',
        'exception_status_code' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Store a new lifecycle logger record.
     *
     * @param \Illuminate\Http\Request $request
     * @param $response
     * @return TFHInc\LifecycleLogger\Models
     */
    public static function store($attributes)
    {
        $lifecycle_logger_model = new self;
        $lifecycle_logger_model->request_id = data_get($attributes, 'request_id');
        $lifecycle_logger_model->request_finger_print = data_get($attributes, 'request_finger_print');
        $lifecycle_logger_model->request_method = data_get($attributes, 'request_method');
        $lifecycle_logger_model->request_root_url = data_get($attributes, 'request_root_url');
        $lifecycle_logger_model->request_url = data_get($attributes, 'request_url');
        $lifecycle_logger_model->request_full_url = data_get($attributes, 'request_full_url');
        $lifecycle_logger_model->request_route_parameters = collect(data_get($attributes, 'request_route_parameters'))->toJSON();
        $lifecycle_logger_model->request_query_string_parameters = collect(data_get($attributes, 'request_query_string_parameters'))->toJSON();
        $lifecycle_logger_model->request_payload = collect(data_get($attributes, 'request_payload'))->toJSON();
        $lifecycle_logger_model->request_headers = collect(data_get($attributes, 'request_headers'))->toJSON();
        $lifecycle_logger_model->request_secure = data_get($attributes, 'request_secure');
        $lifecycle_logger_model->request_ip = data_get($attributes, 'request_ip');
        $lifecycle_logger_model->request_user_agent = data_get($attributes, 'request_user_agent');
        $lifecycle_logger_model->request_time_start_ms = data_get($attributes, 'request_time_start_ms');
        $lifecycle_logger_model->request_time_end_ms = data_get($attributes, 'request_time_end_ms');
        $lifecycle_logger_model->request_time_delta_ms = data_get($attributes, 'request_time_delta_ms');
        $lifecycle_logger_model->response_status_code = data_get($attributes, 'response_status_code');
        $lifecycle_logger_model->response_success_status_code = data_get($attributes, 'response_success_status_code');
        $lifecycle_logger_model->response_exception_status_code = data_get($attributes, 'response_exception_status_code');
        $lifecycle_logger_model->save();
    }
}
