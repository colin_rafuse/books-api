<?php

namespace TFHInc\LifecycleLogger;

// Models
use TFHInc\LifecycleLogger\Models\LifecycleLogger as LifecycleLoggerModel;

// Exceptions
//use TFHInc\LifecycleLogger\Exceptions\InvalidCommandOptionException;

class LifecycleLogger
{
    /**
     * Illuminate Request Instance
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Illuminate Response Instance
     *
     * @var \Illuminate\Http\Response
     */
    protected $response;

    /**
     * LifecycleLogger attributes
     *
     * @var array
     */
    protected $attributes;

    /**
     * Construct the LifecycleLogger class
     *
     * @return void
     */
    public function __construct(\Illuminate\Http\Request $request, \Illuminate\Http\Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Set the Attributes
     *
     * @return void
     */
    protected function setAttributes()
    {
        // Filter Request Payload for sensitive properties
        $filtered_payload = collect(json_decode(request()->getContent(), true))->reject(function ($value, $key) {
            return str_contains($key, "password");
        });

        $this->attributes = [
            'request_id' => uniqid(),
            'request_finger_print' => $this->request->fingerprint(),
            'request_method' => $this->request->method(),
            'request_root_url' => $this->request->root(),
            'request_url' => $this->request->url(),
            'request_full_url' => $this->request->fullUrl(),
            'request_route_parameters' => $this->request->route()->parameters(),
            'request_query_string_parameters' => $this->request->query(),
            'request_payload' => $filtered_payload ?? null,
            'request_headers' => collect($this->request->headers->all())->keyBy(0),
            'request_secure' => $this->request->secure(),
            'request_ip' => $this->request->ip(),
            'request_user_agent' => $this->request->userAgent(),
            'request_time_start_ms' => constant('LARAVEL_START'),
            'request_time_end_ms' => microtime(true),
            'request_time_delta_ms' => round(microtime(true) - constant('LARAVEL_START'),3) * 10000,
            'response_status_code' => $this->response->status(),
            'response_success_status_code' => (starts_with($this->response->status(), '2') ? true : false),
            'response_exception_status_code' => (!starts_with($this->response->status(), '2') ? true : false),
        ];
    }

    /**
     * Get the Attributes
     *
     * @return array $attributes
     */
    protected function getAttributes() :array
    {
        return $this->attributes;
    }

    /**
     * Gather and persist the Lifecycle data to the database.
     *
     * @return void
     */
    public function log()
    {
        $this->setAttributes();
        LifecycleLoggerModel::store($this->getAttributes());
    }
}