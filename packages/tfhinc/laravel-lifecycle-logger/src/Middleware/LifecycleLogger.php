<?php

namespace TFHInc\LifecycleLogger\Middleware;

use Closure;

// Services
use TFHInc\LifecycleLogger\LifecycleLogger as LifecycleLoggerService;

class LifecycleLogger
{
    /**
     * The LifecycleLogger instance.
     *
     * @var TFHInc\LifecycleLogger\LifecycleLogger $lifecycle_logger
     */
    protected $lifecycle_logger;

    /**
     * Construct the LifecycleLogger Middleware class
     *
     * @return void
     */
    public function __construct(LifecycleLoggerService $lifecycle_logger)
    {
        $this->lifecycle_logger = $lifecycle_logger;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Terminate an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $this->lifecycle_logger->log();
    }
}