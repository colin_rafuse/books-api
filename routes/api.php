<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API Root
    Route::get('/', 'WelcomeController');

// Books
    Route::get('/books/search/{query}', 'BookController@search');
    Route::get('/books', 'BookController@index');
    Route::post('/books', 'BookController@store');
    Route::get('/books/{id}', 'BookController@show');
    Route::put('/books/{id}', 'BookController@update');
    Route::delete('/books/{id}', 'BookController@destroy');

// Book Authors
    Route::get('/authors/search/{query}', 'AuthorController@search');
    Route::get('/authors', 'AuthorController@index');
    Route::post('/authors', 'AuthorController@store');
    Route::get('/authors/{id}', 'AuthorController@show');
    Route::put('/authors/{id}', 'AuthorController@update');
    Route::delete('/authors/{id}', 'AuthorController@destroy');

// Book Publishers
    Route::get('/publishers', 'PublisherController@index');
    Route::post('/publishers', 'PublisherController@store');
    Route::get('/publishers/{id}', 'PublisherController@show');
    Route::put('/publishers/{id}', 'PublisherController@update');
    Route::delete('/publishers/{id}', 'PublisherController@destroy');

// Book Types
    Route::get('/types', 'TypeController@index');
    Route::post('/types', 'TypeController@store');
    Route::get('/types/{id}', 'TypeController@show');
    Route::put('/types/{id}', 'TypeController@update');
    Route::delete('/types/{id}', 'TypeController@destroy');

// Book Genres
    Route::get('/genres', 'GenreController@index');
    Route::post('/genres', 'GenreController@store');
    Route::get('/genres/{id}', 'GenreController@show');
    Route::put('/genres/{id}', 'GenreController@update');
    Route::delete('/genres/{id}', 'GenreController@destroy');

// Book Formats
    Route::get('/formats', 'FormatController@index');
    Route::post('/formats', 'FormatController@store');
    Route::get('/formats/{id}', 'FormatController@show');
    Route::put('/formats/{id}', 'FormatController@update');
    Route::delete('/formats/{id}', 'FormatController@destroy');

// Book Languages
    Route::get('/languages', 'LanguageController@index');
    Route::post('/languages', 'LanguageController@store');
    Route::get('/languages/{id}', 'LanguageController@show');
    Route::put('/languages/{id}', 'LanguageController@update');
    Route::delete('/languages/{id}', 'LanguageController@destroy');
