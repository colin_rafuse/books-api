<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Domain Logic
    |--------------------------------------------------------------------------
    |
    | A configuration file to store domain specific logic.
    |
    */

    'pagination' => [

        'items_per_page' => 10

    ],

    'cache' => [

        'key_tty' => 60

    ],

];