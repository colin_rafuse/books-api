<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lifecycle Logger
    |--------------------------------------------------------------------------
    |
    | Set the configuration for the lifecycle logger
    |
    */

    /*
     * The DB connection to persist the lifecycle logger data.
     *
     */
    'db_connection' => env('DB_CONNECTION', 'mysql'),

    /*
     * The DB table to persist the lifecycle logger data.
     *
     */
    'db_table' => 'lifecycle_logger',

];
